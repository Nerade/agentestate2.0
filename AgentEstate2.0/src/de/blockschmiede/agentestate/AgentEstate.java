package de.blockschmiede.agentestate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.avaje.ebean.Query;
import com.iCo6.system.Holdings;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import de.blockschmiede.agentestate.command.AdminHandler;
import de.blockschmiede.agentestate.command.CreateHandler;
import de.blockschmiede.agentestate.command.CustomExecutor;
import de.blockschmiede.agentestate.command.HelpHandler;
import de.blockschmiede.agentestate.command.SellHandler;
import de.blockschmiede.agentestate.command.StartHandler;
import de.blockschmiede.agentestate.estate.Estate;
import de.blockschmiede.agentestate.localization.Localization;
import de.blockschmiede.agentestate.persistence.EstateBean;
import de.blockschmiede.agentestate.persistence.IPersistence;
import de.blockschmiede.agentestate.persistence.PersistenceDatabase;
import de.blockschmiede.agentestate.persistence.PlayerBean;
import de.blockschmiede.agentestate.persistence.RentableBean;
import de.blockschmiede.agentestate.util.*;

public class AgentEstate extends JavaPlugin { // Hauptklasse des Plugins

	// #########Initialisierungen##################

	private final AgentEstateBlockListener BlockListener = new AgentEstateBlockListener(
			this);

	public enum Commands {
		CREATE, CREATERENT, SELL, RELEASE, REPAIR, MULTI, MULTIRENT;
	}

	public HashMap<String, Integer> Price = new HashMap<String, Integer>();
	public HashMap<String, String> Seller = new HashMap<String, String>();
	public HashMap<String, String> Region_ID = new HashMap<String, String>();
	public HashMap<String, Integer> confirm = new HashMap<String, Integer>();
	public HashMap<String, Integer> Price_multi = new HashMap<String, Integer>();
	public HashMap<String, Integer> Period = new HashMap<String, Integer>();
	public HashMap<String, Integer> gs_start = new HashMap<String, Integer>();
	public HashMap<String, Commands> Command = new HashMap<String, Commands>();

	public WorldGuardPlugin worldguard;
	public IPersistence persistenceHandler;
	public PermissionManager permissionManager;
	public Localization locMan;

	public final SignUtil sign_util = new de.blockschmiede.agentestate.util.SignUtil(
			this);
	public final Util util = new de.blockschmiede.agentestate.util.Util(this);
	public final FileUtil file_util = new de.blockschmiede.agentestate.util.FileUtil(
			this);
	boolean startup = true;

	// #########Ende: Initialisierungen##################

	Logger log = Logger.getLogger("Minecraft");

	@Override
	public void onDisable() { // onDisable-Funktion: Daten werden gespeichert
								// und alle Variablen werden freigegeben

		Price.clear();
		Region_ID.clear();
		Seller.clear();
		Price = null;
		Region_ID = null;
		Seller = null;
		Price_multi = null;
		gs_start = null;
		log = null;
		worldguard = null;
		permissionManager = null;
	}

	@Override
	public void onEnable() {

		PluginManager pm = this.getServer().getPluginManager();
		PluginDescriptionFile pdfFile = this.getDescription();

		setupConfiguration();

		this.persistenceHandler = new PersistenceDatabase(this);

		setupPermissions();

		worldguard = getWorldGuard();

		pm.registerEvents(BlockListener, this);

		locMan = new Localization("en_US", this);

		this.getServer().getScheduler()
				.scheduleSyncDelayedTask(this, new Runnable() {

					@Override
					public void run() {
						log.info(locMan.get("repairedEntries", clearDB()));
					}
				}, 200L);

	}

	private void setupConfiguration() {

		Map<String, Object> configNodes = new HashMap<String, Object>();

		configNodes.put("short_cur", "PT");
		configNodes.put("currency", "Platin");
		configNodes.put("price_per_block", 20);
		configNodes.put("inactive_days_allowed", 20);
		configNodes.put("default_period", 7);
		configNodes.put("global_account_name", "Bank");
		configNodes.put("save_rent", true);
		configNodes.put("save_Buy", true);
		configNodes.put("gsstart_limit", 1);
		configNodes.put("mysql_user", "");
		configNodes.put("mysql_pw", "");
		configNodes.put("mysql_db", "");

		for (final Entry<String, Object> e : configNodes.entrySet())
			if (!this.getConfig().contains(e.getKey()))
				this.getConfig().addDefault(e.getKey(), e.getValue());
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}

	private void setupPermissions() { // Methode zur Verbindung mit dem
										// Rechteverwaltungsplugin PermissionsEx

		if (permissionManager != null) {
			return;
		}

		Plugin permissionsPlugin = this.getServer().getPluginManager()
				.getPlugin("PermissionsEx");
		// Plugin permissionsPlugin =
		// this.getServer().getPluginManager().getPlugin("Permissions");

		if (permissionsPlugin == null) {
			log.info("Permission system not detected, defaulting to OP");
			return;
		}

		permissionManager = PermissionsEx.getPermissionManager();
		log.info("Found and will use plugin "
				+ ((PermissionsEx) permissionsPlugin).getDescription()
						.getFullName());

	}

	@Override
	public void installDDL() {
		// make the installDLL() method visible for the persistence layer
		super.installDDL();
	}

	@Override
	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> beans = new LinkedList<Class<?>>();

		beans.add(EstateBean.class);
		beans.add(RentableBean.class);
		beans.add(PlayerBean.class);

		return beans;
	}

	public int clearDB() { // Defekte Einträge in der Datenbank löschen
		int counter = 0;
		Query<EstateBean> query = this.getDatabase().find(EstateBean.class);
		List<EstateBean> list = query.findList();

		for (EstateBean e : list) {
			int x = e.getX();
			int y = e.getY();
			int z = e.getZ();
			String world = e.getWorld();
			Block block = this.getServer().getWorld(world).getBlockAt(x, y, z);
			int maxTime = this.getConfig().getInt("inactive_days_allowed", 20) * 24 * 3600 * 1000;

			if (!(block.getTypeId() == 63) && !(block.getTypeId() == 68)) {
				this.getDatabase().delete(e);
				counter++;
			} else {
				OfflinePlayer p_off = this.getServer().getOfflinePlayer(
						e.getOwner());
				long time_off = System.currentTimeMillis()
						- p_off.getLastPlayed();
				if (time_off > maxTime) {
					Estate es = new Estate(e, this);
					es.setDefaultPrice();
					es.sell();
					this.persistenceHandler.saveEstate(es);
					counter++;
				}

				Sign sign = (Sign) block.getState();
				if (sign.getLine(1).equals("")
						|| !sign.getLine(1).substring(2)
								.equals(e.getRegion_id())) {
					if (e.getPrice() == -1) {
						this.sign_util.updateSign(sign, "", e.getRegion_id(),
								e.getOwner(), "");
					} else {
						this.sign_util.updateSign(sign, locMan.get("forSale"),
								e.getRegion_id(), e.getOwner(),
								String.valueOf(e.getPrice()));
					}
					counter++;
				}
			}
		}
		return counter;
	}

	private boolean updatePrices(Player p) { // Methode zur Aktualisierung der
												// Grundstückspreise

		return true;
	}

	private void registerCommandExecutors() {
/*		getCommand("sell").setExecutor(new SellHandler(this));
		getCommand("release").setExecutor(new SellHandler(this));
		getCommand("start").setExecutor(new StartHandler(this));
		getCommand("restart").setExecutor(new StartHandler(this));
		getCommand("create").setExecutor(new CreateHandler(this));
		getCommand("createrent").setExecutor(new CreateHandler(this));
		getCommand("repair").setExecutor(new CreateHandler(this));
		getCommand("multimode").setExecutor(new CreateHandler(this));
		getCommand("multirent").setExecutor(new CreateHandler(this));
		getCommand("clear").setExecutor(new AdminHandler(this));
		getCommand("list").setExecutor(new AdminHandler(this));
		getCommand("update").setExecutor(new AdminHandler(this));
		getCommand("help").setExecutor(new HelpHandler(this));*/
		
		getCommand("gs").setExecutor(new CustomExecutor(this));
	}

	/*	private java.util.Vector<String> list_gs(String[] args) { // Hilfs-Methode
																// zur Suche in
																// der Datenbank

		String par[] = { "%", "%", "%", "%" };
		for (int i = 0; i < args.length - 1; i++) {
			if (!args[i + 1].equals("*")) {
				par[i] = args[i + 1];
			}
		}

		// TODO Auto-generated method stub
		return getList(par[0], par[1], par[2], par[3]);
	}

	private java.util.Vector<String> getList(String region, String owner, // Haupt-Methode
																			// zur
																			// Suche
																			// in
																			// der
																			// Datenbank
			String seller, String price) {

		java.util.Vector<String> results = new java.util.Vector<String>();
		try {
			PreparedStatement ps = mysql
					.openConnection()
					.prepareStatement(
							"SELECT * FROM agentestate WHERE Owner LIKE ? AND Seller LIKE ? AND Price LIKE ? AND RegionID LIKE ?");
			ps.setString(1, owner);
			ps.setString(2, seller);
			ps.setString(3, price);
			ps.setString(4, region);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				results.add(rs.getString("RegionID") + "//"
						+ rs.getString("Owner") + "//" + rs.getString("Seller")
						+ "//" + rs.getString("Price"));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}*/

	private WorldGuardPlugin getWorldGuard() { // Schnittstelle zum
												// WorldGuard-Plugin

		Plugin worldGuard = getServer().getPluginManager().getPlugin(
				"WorldGuard");

		// WorldGuard may not be loaded
		if (worldGuard == null || !(worldGuard instanceof WorldGuardPlugin)) {
			return null; // Maybe you want throw an exception instead
		}

		return (WorldGuardPlugin) worldGuard;

	}

}
