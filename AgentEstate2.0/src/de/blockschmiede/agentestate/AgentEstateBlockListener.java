package de.blockschmiede.agentestate;

import java.text.SimpleDateFormat;

import org.bukkit.ChatColor;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import de.blockschmiede.agentestate.AgentEstate.Commands;
import de.blockschmiede.agentestate.estate.Estate;
import de.blockschmiede.agentestate.estate.Rentable;
import de.blockschmiede.agentestate.persistence.EstateBean;

public class AgentEstateBlockListener implements Listener { // Blocklistener des
															// Plugins

	public AgentEstate plugin;

	public AgentEstateBlockListener(AgentEstate instance) {
		plugin = instance;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent block) { // Sobald ein Block (in
														// diesem Fall nur
														// Schild) zerst�rt wird

		if ((block.getBlock().getTypeId() == 63)
				|| (block.getBlock().getTypeId() == 68)) {
			Sign sign = (Sign) block.getBlock().getState();
			if (plugin.sign_util.isMarkler(sign)) {
				Player p = block.getPlayer();
				if (!plugin.permissionManager.has(p, "agentestate.remove")) {
					block.setCancelled(true);
					sign.update();
					p.sendMessage(ChatColor.RED
							+ plugin.locMan.get("noRightsRemove"));
				} else {
					if (p.getGameMode().toString().equals("SURVIVAL")
							|| p.isSneaking()) {

						Estate curEstate = plugin.persistenceHandler
								.loadEstate(sign.getLine(1).substring(2));

						if (curEstate.remove()) {
							p.sendMessage(plugin.locMan.get("successRemoved"));
						} else {
							p.sendMessage(plugin.locMan.get("errorRemoved"));
						}
					} else {
						block.setCancelled(true);
						p.sendMessage(plugin.locMan.get("requestSurvival"));
					}

				}
			} else if (block.getPlayer().getItemInHand().getTypeId() == 287) {

				if (plugin.permissionManager.has(block.getPlayer(),
						"agentestate.create")) {
					block.setCancelled(true);
					plugin.getServer()
							.getPluginManager()
							.callEvent(
									new BlockDamageEvent(block.getPlayer(),
											block.getBlock(), block.getPlayer()
													.getItemInHand(), false));

				}

			}

		}

	}

	@EventHandler
	public void onBlockDamage(BlockDamageEvent block) { // Sobald ein Block (in
														// diesem Fall nur
														// Schild) besch�digt
														// wird
		Player p = block.getPlayer();

		// ################ SCHILD ANGEKLICKT ###################

		if ((block.getBlock().getTypeId() == 63)
				|| (block.getBlock().getTypeId() == 68)) {

			if (plugin.Command.get(p.getName()) != null) {
				if (plugin.Command.get(p.getName()).equals(Commands.CREATE)) {

					if (p.getItemInHand().getTypeId() == 287) {

						block.setCancelled(true);

						Estate newEstate = plugin.persistenceHandler
								.loadEstate(plugin.Region_ID.get(p.getName()));
						if (newEstate == null) {
							newEstate = new Estate(plugin.Region_ID.get(p
									.getName()), plugin);
						}

						Sign sign = (Sign) block.getBlock().getState();
						newEstate.setOwner(plugin.getConfig().getString("Bank",
								"Bank"));
						newEstate.setPrice(plugin.Price.get(p.getName()));
						newEstate.create(sign);
					}

					plugin.Price.remove(p.getName());
					plugin.Seller.remove(p.getName());
					plugin.Region_ID.remove(p.getName());

				} else if (plugin.Command.get(p.getName()).equals(
						Commands.CREATERENT)) {

					if (p.getItemInHand().getTypeId() == 287) {

						block.setCancelled(true);

						Rentable newRent = plugin.persistenceHandler
								.loadRentable(plugin.Region_ID.get(p.getName()));
						if (newRent == null) {
							newRent = new Rentable(plugin.Region_ID.get(p
									.getName()), plugin);
						}

						Sign sign = (Sign) block.getBlock().getState();
						newRent.setOwner(plugin.getConfig().getString("Bank",
								"Bank"));
						newRent.setPrice(-1);
						if (plugin.Period.get(p.getName()) != null) {
							newRent.setPeriod(plugin.Period.get(p.getName()));
							plugin.Period.remove(p.getName());
						} else {
							newRent.setDefaultRent();
						}
						newRent.setRent(plugin.Price.get(p.getName()));
						newRent.create(sign);

					}

					plugin.Price.remove(p.getName());
					plugin.Seller.remove(p.getName());
					plugin.Region_ID.remove(p.getName());

				} else if (plugin.Command.get(p.getName()).equals(
						Commands.REPAIR)) {

					if (p.getItemInHand().getTypeId() == 287) {
						block.setCancelled(true);
						// Estate newEstate = new Estate(plugin.Region_ID.get(p
						// .getName()), false, plugin);
						// Sign sign = (Sign) block.getBlock().getState();
						// newEstate.setOwner(plugin.Seller.get(p.getName()));
						// newEstate.setSeller("");
						// newEstate.repair(sign,
						// plugin.Seller.get(p.getName()));
						p.sendMessage("T.B.D");

					}

					plugin.Price.remove(p.getName());
					plugin.Seller.remove(p.getName());
					plugin.Region_ID.remove(p.getName());

				} else if (plugin.Command.get(p.getName()).equals(
						Commands.MULTI)) {

					if (p.getItemInHand().getTypeId() == 287) {

						block.setCancelled(true);
						
						ProtectedRegion auto_region = plugin.sign_util
								.isRegionNear((Sign) block.getBlock()
										.getState());
						if (auto_region != null) {
							Estate newEstate = new Estate(auto_region.getId(), plugin);
							if (plugin.Price_multi.get(p.getName()) != -1) {

								newEstate.setOwner(plugin.getConfig()
										.getString("Bank", "Bank"));
								newEstate.setPrice(plugin.Price_multi.get(p
										.getName()));
								newEstate.create((Sign) block.getBlock()
										.getState());

					
								
								// if (plugin.create(p, (Sign)
								// block.getBlock()
								// .getState(), auto_region.getId(),
								// plugin.Price_multi.get(p.getName()))) {
								// p.sendMessage("Markler erstellt!");
								// }
							} else {
								int area = plugin.util.getArea(auto_region);
								newEstate.setOwner(plugin.getConfig()
										.getString("Bank", "Bank"));
								newEstate.setPrice(area
										* plugin.getConfig().getInt(
												"price_per_block", 20));
								newEstate.create((Sign) block.getBlock()
										.getState());
							}
						}
					}

				} else if (plugin.Command.get(p.getName()).equals(
						Commands.MULTIRENT)) {

					if (plugin.permissionManager.has(p,
							"agentestate.createrent")) {

						if (p.getItemInHand().getTypeId() == 287) {

							block.setCancelled(true);
							ProtectedRegion auto_region = plugin.sign_util
									.isRegionNear((Sign) block.getBlock()
											.getState());
							if (auto_region != null) {
								Rentable newRent = new Rentable(
										auto_region.getId(), plugin);
								if (plugin.Price_multi.get(p.getName()) != -1) {

									newRent.setOwner(plugin.getConfig()
											.getString("Bank", "Bank"));
									newRent.setPrice(-1);
									newRent.setRent(plugin.Price_multi.get(p
											.getName()));

									// if (plugin.create(p, (Sign)
									// block.getBlock()
									// .getState(), auto_region.getId(),
									// plugin.Price_multi.get(p.getName()))) {
									// p.sendMessage("Markler erstellt!");
									// }
								} else {
									int area = plugin.util.getArea(auto_region);
									newRent.setOwner(plugin.getConfig()
											.getString("Bank", "Bank"));
									newRent.setPrice(-1);
									newRent.setRent(area
											* plugin.getConfig().getInt(
													"price_per_block", 20) / 5);
								}
								
								newRent.create((Sign) block
										.getBlock().getState());
								}
							}
						}

					}

				} else if (plugin.Command.get(p.getName()).equals(
						Commands.RELEASE)) {
					if (plugin.permissionManager.has(p, "agentestate.sell")) {
						if (!plugin.sign_util.isMarkler((Sign) block.getBlock()
								.getState())) {

							p.sendMessage("Das Schild ist kein Makler!");
							return;
						}
						Rentable curEstate = plugin.persistenceHandler.loadRentable(((Sign) block
								.getBlock().getState()).getLine(1));
						if (!curEstate.getRenter().equals(p.getName())) {
							p.sendMessage("Das Grundst�ck geh�rt dir nicht!");
							plugin.Command.remove(p.getName());
							return;
						}
						if (curEstate.release()) {
							p.sendMessage(plugin.locMan.get("releaseEstate"));
						}

					}

				} else if (plugin.Command.get(p.getName())
						.equals(Commands.SELL)) {

					if (plugin.permissionManager.has(p, "agentestate.sell")) {
						// System.out.println("Debug_point_1");
						if (!plugin.sign_util.isMarkler((Sign) block.getBlock()
								.getState())) {

							p.sendMessage("Das Schild ist kein Makler!");
							return;
						}
						Estate curEstate = plugin.persistenceHandler.loadEstate(((Sign) block
								.getBlock().getState()).getLine(1));
						// System.out.println(p.getName());
						// System.out.println(newEstate.getOwner());
						if (!curEstate.getOwner().equals(p.getName())) {
							p.sendMessage("Das Grundst�ck geh�rt dir nicht!");
							plugin.Command.remove(p.getName());
							return;
						}
						curEstate.setPrice(plugin.Price.get(p.getName()));
						if (curEstate.sell()) {
							p.sendMessage(plugin.locMan.get("forSaleSuccess"));
						}
						plugin.Price.remove(p.getName());
						plugin.Region_ID.remove(p.getName());

						// plugin.sell(p, (Sign) block.getBlock().getState(),
						// plugin.Price.get(p.getName()));

					}
				}

				// ################# KAUFEN ##################

			} else if (plugin.permissionManager.has(p, "agentestate.buy")) {
				Sign sign = (Sign) block.getBlock().getState();
				Rentable curRent;
				Estate curEstate = plugin.persistenceHandler.loadEstate(sign.getLine(1).substring(2));
				if(curEstate == null){
					curRent = plugin.persistenceHandler.loadRentable(sign.getLine(1).substring(2));
				}
				long timestamp = System.currentTimeMillis()+5000;
//				int time = (int) (p.getLocation().getX() * 4);
//				loc += p.getLocation().getZ();
				
				if (curRent != null) {
					if (curRent.getRenter() != "") { // Rentable is rented
						
						if (sign.getLine(2).equals(p.getName())) {  // Renter Clicked
							
							if ((plugin.confirm.get(p.getName()) == null)
									|| (plugin.confirm.get(p.getName()) <= System.currentTimeMillis())) {
								p.sendMessage("Du hast bezahlt bis zum "
										+ ChatColor.GREEN
										+ new SimpleDateFormat(
												"dd/MM/yyyy HH:mm")
												.format(curRent
														.getExpiration()));
								p.sendMessage("Klicke ein weiteres mal zum Verl�ngern um "
										+ ChatColor.GREEN
										+ curRent.getPeriod()
										+ " Tage"
										+ ChatColor.WHITE + "...");
								plugin.confirm.put(p.getName(), loc);

							} else { // Renter clicked 2nd time
								if (curRent.rent(p)) {
									p.sendMessage("Du hast deine Miete erfolgreich verl�ngert!");
								}
							}
							
						} else { // Non-Renter clicked

							p.sendMessage("Das Grundst�ck ist bereits vermietet!");
							p.sendMessage("L�uft ab am "
									+ ChatColor.GREEN
									+ new SimpleDateFormat(
											"dd/MM/yyyy HH:mm")
											.format(curRent.getExpiration()));
						}
								
					} else {  //Rentable is free
						
						if ((plugin.confirm.get(p.getName()) == null)
								|| (plugin.confirm.get(p.getName()) <= System.currentTimeMillis())) {
								p.sendMessage("Klicke ein weiteres mal zur Best�tigung um es f�r "
										+ ChatColor.GREEN
										+ curRent.getPeriod()
										+ " Tage "
										+ ChatColor.WHITE + "zu mieten...");
						} else {
							if (curRent.rent(p)) {
								p.sendMessage("Du hast das Grundst�ck nun gemietet!");
							}
						}
						
					}
				} else if(curEstate != null){
					if(curEstate.getPrice()==-1){
						if (sign.getLine(2).equals(block.getPlayer().getName())) {
							p.sendMessage("Um dein Grundst�ck zu verkaufen tippe /gs sell <Preis> ein!");
						} else {
							p.sendMessage("Das Grundst�ck steht nicht zum Verkauf!");
						}
					} else {
						
						
					}
				}
						} else { // Estate is available

							if ((plugin.confirm.get(p.getName()) == null)
									|| (plugin.confirm.get(p.getName()) != temp)) {
								if (curEstate.isRentable()) {
									p.sendMessage("Klicke ein weiteres mal zur Best�tigung um es f�r "
											+ ChatColor.GREEN
											+ curEstate.getPeriod()
											+ " Tage "
											+ ChatColor.WHITE + "zu mieten...");
								} else {
									p.sendMessage("Klicke ein weiteres mal zur Best�tigung des Kaufes...");
								}
								plugin.confirm.put(p.getName(), temp);
							} else {
								// System.out.println("Debug_point_2");

								if (curEstate.isRentable()) {
									if (curEstate.rent(p)) {
										p.sendMessage("Du hast das Grundst�ck nun gemietet!");
									}
								} else {
									if (curEstate.buy(p)) {
										p.sendMessage("Das Grundst�ck geh�rt nun dir!");
									}
								}
								plugin.confirm.remove(p.getName());

							}
						}
					} else {
						if (sign.getLine(2).equals(block.getPlayer().getName())) {
							p.sendMessage("Um dein Grundst�ck zu verkaufen tippe /gs sell <Preis> ein!");
						} else {
							
						}
					}
				}
			} else {
				p.sendMessage(ChatColor.RED
						+ "Du hast nicht die n�tigen Rechte um das zu tun!");
			}
			if (plugin.Command.get(p.getName()) != Commands.MULTI
					&& plugin.Command.get(p.getName()) != Commands.MULTIRENT) {
				plugin.Command.remove(p.getName());
			}
		}
	}
}
