package de.blockschmiede.agentestate.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.blockschmiede.agentestate.AgentEstate;

public class AdminHandler {
	
	private AgentEstate plugin;
	public AdminHandler(AgentEstate plugin){
		this.plugin = plugin;
	}

	public Boolean clear(CommandSender sender, String[] args) {

		if (plugin.permissionManager.has((Player) sender,
				"agentestate.admin")) {

			sender.sendMessage(plugin.locMan.get("repairedEntries",plugin.clearDB()));
			return true;
		}
		return false;
	}
	public Boolean reload(CommandSender sender, String[] args) {
		
		if (plugin.permissionManager.has((Player) sender,
				"agentestate.admin")) {

			plugin.reloadConfig();
			return true;
		}
		return false;
	}
	
	public boolean list(CommandSender sender,String[] args){
		/*if (plugin.permissionManager.has((Player) sender,
				"agentestate.list")) {
			java.util.Vector<String> results = list_gs(args);
			if (!results.isEmpty()) {
				if (results.capacity() <= 10) {
					sender.sendMessage(ChatColor.GREEN
							+ "Results of the search:");
					for (String gs : results) {
						sender.sendMessage(gs);
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "Too much results! Please use better filter!");
				}
			} else {
				sender.sendMessage(ChatColor.RED
						+ "No results found!");

			}
			return true;

		}*/
		return false;
	}
}
