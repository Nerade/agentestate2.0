package de.blockschmiede.agentestate.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.AgentEstate.Commands;

public class CreateHandler implements CommandExecutor{

	private AgentEstate plugin;
	public CreateHandler(AgentEstate plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
			String[] arg3) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Boolean create(CommandSender sender, String[] args) {

		if (args.length == 3) {
			if (plugin.permissionManager.has((Player) sender,
					"agentestate.create")) {
				if (Integer.valueOf(args[2]) >= 0) {

					plugin.Price.put(sender.getName(),
							Integer.valueOf(args[2]));

					plugin.Region_ID.put(sender.getName(), args[1]);
					plugin.Command.put(sender.getName(),
							Commands.CREATE);
					sender.sendMessage(plugin.locMan.get("clickForBind"));
					return true;
				}
			}
		}
		return false;
	}
	
	public Boolean createrent(CommandSender sender, String[] args) {
	
		if (args.length == 3 || args.length == 4) {
			if (plugin.permissionManager.has((Player) sender,
					"agentestate.create")) {
				if (Integer.valueOf(args[2]) >= 0) {

					plugin.Price.put(sender.getName(),
							Integer.valueOf(args[2]));

					plugin.Region_ID.put(sender.getName(), args[1]);
					if (args.length == 4) {
						plugin.Period.put(sender.getName(),
								Integer.valueOf(args[3]));
					}
					plugin.Command.put(sender.getName(),
							Commands.CREATERENT);
					sender.sendMessage(plugin.locMan.get("clickForBind"));
					return true;
				}
			}
		}
		return false;		
	}
	public Boolean repair(CommandSender sender, String[] args) {

		if (args.length == 3) {
			if (plugin.permissionManager.has((Player) sender,
					"agentestate.create")) {

				plugin.Seller.put(sender.getName(), args[2]);
				plugin.Region_ID.put(sender.getName(), args[1]);
				plugin.Command.put(sender.getName(), Commands.REPAIR);
				sender.sendMessage(plugin.locMan.get("clickForBind"));
				return true;
			}
		}
		return false;
		
	}
	public Boolean multimode(CommandSender sender, String[] args) {

		if (args.length == 2) {

			if (plugin.permissionManager.has((Player) sender,
					"agentestate.create")) {

				if (args[1].equals("off")) {

					plugin.Price_multi.remove(sender.getName());
					plugin.Command.remove(sender.getName());
					sender.sendMessage(plugin.locMan.get("deactivatedMultiMode"));
					return true;
				} else {

					plugin.Price_multi.put(sender.getName(),
							Integer.valueOf(args[1]));
					sender.sendMessage(plugin.locMan.get("activatedMultiMode",args[1]));
					plugin.Command.put(sender.getName(),
							Commands.MULTI);
					return true;
				}
			}
		}
		return false;
	}
	public Boolean multirent(CommandSender sender, String[] args) {

		if (args.length == 2) {

			if (plugin.permissionManager.has((Player) sender,
					"agentestate.create")) {

				if (args[1].equals("off")) {

					plugin.Price_multi.remove(sender.getName());
					plugin.Command.remove(sender.getName());
					sender.sendMessage(plugin.locMan.get("deactivatedMultiRent"));
					return true;
				} else {

					plugin.Price_multi.put(sender.getName(),
							Integer.valueOf(args[1]));
					sender.sendMessage(plugin.locMan.get("activatedMultiRent",args[1]));
					plugin.Command.put(sender.getName(),
							Commands.MULTIRENT);
					return true;
				}
			}
		}
		return false;
	}
}
