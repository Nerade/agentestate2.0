package de.blockschmiede.agentestate.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.AgentEstate.Commands;

public class CustomExecutor implements CommandExecutor {

	private AgentEstate plugin;
	private AdminHandler adminHandler;
	private CreateHandler createHandler;
	private SellHandler sellHandler;
	private StartHandler startHandler;
	private HelpHandler helpHandler;

	public CustomExecutor(AgentEstate plugin) {
		this.plugin = plugin;
		adminHandler = new AdminHandler(plugin);
		createHandler = new CreateHandler(plugin);
		startHandler = new StartHandler(plugin);
		sellHandler = new SellHandler(plugin);
		helpHandler = new HelpHandler(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {

		Boolean success = false;

		if (cmd.getLabel().equalsIgnoreCase("agentestate")
				|| cmd.getLabel().equalsIgnoreCase("gs")) {
			if (sender instanceof Player) {
				if (args.length > 0) {

					if (args[0].equalsIgnoreCase("sell")) {

						success = sellHandler.sell(sender, args);

					} else if (args[0].equalsIgnoreCase("release")) {

						success = sellHandler.release(sender, args);

					} else if (args[0].equalsIgnoreCase("start")) {

						success = startHandler.start(sender, args);

					} /*
					 * else if (args[0].equalsIgnoreCase("restart")) {
					 * 
					 * success = startHandler.restart(sender,args);
					 * 
					 * }
					 */

					// ##### ADMIN Befehle!#############

					else if (args[0].equalsIgnoreCase("create")) {

						success = createHandler.create(sender, args);

					} else if (args[0].equalsIgnoreCase("createrent")) {

						success = createHandler.createrent(sender, args);

					} else if (args[0].equalsIgnoreCase("repair")) {

						success = createHandler.repair(sender, args);

					} else if (args[0].equalsIgnoreCase("multimode")
							|| args[0].equalsIgnoreCase("mm")) {

						success = createHandler.multimode(sender, args);

					} else if (args[0].equalsIgnoreCase("multirent")
							|| args[0].equalsIgnoreCase("mr")) {

						success = createHandler.multirent(sender, args);

					} else if (args[0].equalsIgnoreCase("clear")) {

						success = adminHandler.clear(sender, args);

					}/*
					 * else if (args[0].equalsIgnoreCase("update")) {
					 * 
					 * if (plugin.permissionManager.has((Player) sender,
					 * "agentestate.admin")) {
					 * 
					 * plugin.updatePrices((Player) sender); return true; } }
					 */else if (args[0].equalsIgnoreCase("reload")) {

						success = adminHandler.reload(sender, args);

					} else if (args[0].equalsIgnoreCase("list")) {

						success = adminHandler.list(sender, args);
					}

				} else if (args[0].equalsIgnoreCase("reload")) {

					plugin.reloadConfig();
					return true;

				}
			}
			if (!success) {
				helpHandler.help((Player) sender);
				return true;
			}
		}
		return false;
	}

}
