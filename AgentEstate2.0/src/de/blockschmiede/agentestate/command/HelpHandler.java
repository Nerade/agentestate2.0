package de.blockschmiede.agentestate.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.blockschmiede.agentestate.AgentEstate;

public class HelpHandler {

	private AgentEstate plugin;

	public HelpHandler(AgentEstate plugin) {
		this.plugin = plugin;
	}

	public void help(Player p) { // Routine zur Erzeugung einer formattierten
		// Hilfe-Ausgabe

		p.sendMessage("Command Reference for AgentEstate:");
		if (plugin.permissionManager.has(p, "agentestate.admin")) {

			p.sendMessage(ChatColor.GREEN
					+ "/gs clear "
					+ ChatColor.WHITE
					+ " // Ueberprueft und reinigt Datenbank/Gibt Grundstuecke frei(ADMIN)");
			p.sendMessage(ChatColor.GREEN + "/gs reload " + ChatColor.WHITE
					+ " // Laedt die Config neu(ADMIN)");
			p.sendMessage(ChatColor.GREEN + "/gs update " + ChatColor.WHITE
					+ " // Updatet die Preise (ADMIN)");

		}
		if (plugin.permissionManager.has(p, "agentestate.create")) {

			p.sendMessage(ChatColor.GREEN
					+ "/gs multimode <preis> "
					+ ChatColor.WHITE
					+ " // Aktiviert den Multimode(ADMIN) (Preis -1 = Autopreis)");
			p.sendMessage(ChatColor.GREEN
					+ "/gs multirent <preis> "
					+ ChatColor.WHITE
					+ " // Aktiviert den Multimode f�r Miete(ADMIN) (Preis -1 = Automiete)");
			p.sendMessage(ChatColor.GREEN + "/gs create <region_id> <preis> "
					+ ChatColor.WHITE
					+ " // Erstellt einen neuen Makler (ADMIN)");
			p.sendMessage(ChatColor.GREEN + "/gs repair <region_id> <owner> "
					+ ChatColor.WHITE
					+ " // Ersetzt ein zerstoertes Schild (ADMIN)");

		}
		if (plugin.permissionManager.has(p, "agentestate.createrent")) {
			p.sendMessage(ChatColor.GREEN
					+ "/gs createrent <region_id> <rent> <period>(optional)"
					+ ChatColor.WHITE
					+ " // Erstellt ein mietbares Grundstueck (ADMIN)");

		}
		if (plugin.permissionManager.has(p, "agentestate.list")) {

			p.sendMessage(ChatColor.GREEN
					+ "/gs list <region> <owner> <seller> <price> "
					+ ChatColor.WHITE
					+ " // Sucht nach GS. Soll nach allem gesucht werden '*' verwenden");

		}

		p.sendMessage(ChatColor.GREEN + "/gs sell <preis> " + ChatColor.WHITE
				+ " // Setzt dein Grundstueck zum Verkauf");
		p.sendMessage(ChatColor.GREEN + "/gs release " + ChatColor.WHITE
				+ " // Gibt dein gemietetes Grundstueck wieder frei");
		p.sendMessage(ChatColor.GREEN + "/gs start " + ChatColor.WHITE
				+ " // Bringt dich zu einem freien Grundstueck");
		p.sendMessage(ChatColor.GREEN + "/gs restart " + ChatColor.WHITE
				+ " // Resettet die interne Liste an freien Grundstuecken");

	}

}
