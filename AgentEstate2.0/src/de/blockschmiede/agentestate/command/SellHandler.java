package de.blockschmiede.agentestate.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.AgentEstate.Commands;

public class SellHandler implements CommandExecutor {

	private AgentEstate plugin;

	public SellHandler(AgentEstate agentEstate) {
		this.plugin = agentEstate;
	}

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
			String[] arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean sell(CommandSender sender, String[] args) {

		if (plugin.permissionManager.has((Player) sender, "agentestate.sell")) {
			if (args.length == 2) {
				sender.sendMessage(ChatColor.RED
						+ plugin.locMan.get("warningSell"));
				sender.sendMessage(ChatColor.RED
						+ plugin.locMan.get("promptAccept"));
				return true;
			} else if (args.length == 3) {
				if (args[2].equalsIgnoreCase("accept")) {
					if (Integer.valueOf(args[1]) >= 0) {
						plugin.Price.put(sender.getName(),
								Integer.valueOf(args[1]));
						plugin.Seller.put(sender.getName(), sender.getName());
						plugin.Command.put(sender.getName(), Commands.SELL);
						sender.sendMessage(plugin.locMan.get("clickForSale"));
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean release(CommandSender sender, String[] args) {

		if (plugin.permissionManager.has((Player) sender, "agentestate.sell")) {
			if (args.length == 1) {
				sender.sendMessage(ChatColor.RED
						+ plugin.locMan.get("warningRelease"));
				sender.sendMessage(ChatColor.RED
						+ plugin.locMan.get("promptAccept"));
				return true;
			} else {
				if (args.length == 2) {
					if (args[1].equalsIgnoreCase("accept")) {
						plugin.Command.put(sender.getName(), Commands.RELEASE);
						sender.sendMessage(plugin.locMan.get("clickForRelease"));
						return true;
					}
				}
			}
		}
		return false;
	}

}
