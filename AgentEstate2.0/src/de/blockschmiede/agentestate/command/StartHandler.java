package de.blockschmiede.agentestate.command;

import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.avaje.ebean.Query;
import com.iCo6.system.Holdings;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.persistence.EstateBean;
import de.blockschmiede.agentestate.persistence.PlayerBean;
import de.blockschmiede.agentestate.persistence.RentableBean;

public class StartHandler {
	
	private AgentEstate plugin;
	public StartHandler(AgentEstate plugin){
		this.plugin = plugin;
	}
	
	public boolean start(CommandSender sender, String[] args) {
		
		if (plugin.permissionManager.has((Player) sender,
				"agentestate.start")) {
			Location loc = getGs_rand((Player) sender);
			if (loc != null) {
				((Player) sender).teleport(loc);
				sender.sendMessage(plugin.locMan.get("repeatStart"));
			} else {
				sender.sendMessage(plugin.locMan.get("callForAdmin"));
			}
			return true;

		}
		return false;	
	}
	
	private Location getGs_rand(Player p) {
		int limit = plugin.getConfig().getInt("gsstart_limit");
		if (limit > 0) {
			Query<PlayerBean> query = plugin.getDatabase().find(PlayerBean.class);
			query.where().eq("player_name", p.getName());
			PlayerBean pBean = query.findUnique();
			if (limit <= pBean.getCount()) {
				return null;
			}
		}
		Holdings balance = new Holdings(p.getName());

		Random rand = new Random();

		Object bean=null;

		if (rand.nextBoolean()) {
			Query<EstateBean> query = plugin.getDatabase().find(EstateBean.class);
			query.where().le("price", balance.getBalance());
			List<EstateBean> results = query.findList();
			if (results.size() != 0) {
				bean = results.get(rand.nextInt(results.size()));
			}
		}
		if (bean == null) {
			Query<RentableBean> query2 = plugin.getDatabase().find(
					RentableBean.class);
			query2.where().le("rent", balance.getBalance());
			List<RentableBean> results2 = query2.findList();
			if (results2.size() != 0) {
				bean = results2.get(rand.nextInt(results2.size()));
			}
		}
		if (bean == null) {
			p.sendMessage(plugin.locMan.get("callForAdmin"));
			return null;
		}
		if (bean instanceof EstateBean) {
			return new Location(plugin.getServer().getWorld(((EstateBean)bean).getWorld()),
					((EstateBean)bean).getX(), ((EstateBean)bean).getY(), ((EstateBean)bean).getZ());
		} else {
			return new Location(plugin.getServer().getWorld(((RentableBean)bean).getWorld()),
					((RentableBean)bean).getX(), ((RentableBean)bean).getY(), ((RentableBean)bean).getZ());
		}

	}

/*	public Boolean restart(CommandSender sender, String[] args) {

		if (plugin.permissionManager.has((Player) sender,
				"agentestate.start")) {

			plugin.gs_start.remove(sender.getName());
			sender.sendMessage(plugin.locMan.get("newList"));
			Location loc = getGs_rand((Player) sender);
			((Player) sender).teleport(loc);
			sender.sendMessage("Sollte dir das Grundstück nicht gefallen, gib /gs start erneut ein!");
			return true;

		}
		return false;
		
	}*/

}
