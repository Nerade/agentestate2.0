package de.blockschmiede.agentestate.estate;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.iCo6.system.Holdings;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.LocalWorld;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.persistence.EstateBean;

public class Estate { 

	protected int price, area;
	protected boolean registered;
	protected String region_id, owner, world;
	protected Location location;
	protected Sign sign;
	protected AgentEstate plugin;
	protected ProtectedRegion region;
	
	public Estate(EstateBean bean,AgentEstate plugin){
		this.registered=true;
		this.region_id=bean.getRegion_id();
		this.setArea(bean.getArea());
		this.setOwner(bean.getOwner());
		this.setPrice(bean.getPrice());
		this.setWorld(bean.getWorld());
		this.setLocation(this.plugin.getServer().getWorld(bean.getWorld()),bean.getX(),bean.getY(),bean.getZ());
		//this.fillValues();
	}

	protected void setWorld(String world) {
		this.world=world;
	}

	protected void setLocation(World world, int x, int y, int z) {
		this.location=new Location(world,x,y,z);
		
	}

	public Estate(String region_id, AgentEstate plugin) { 
		this.plugin = plugin;
		this.region_id = region_id;
		this.registered = checkRegistered();
	}

	public Estate(Sign sign, AgentEstate plugin) {
		this(sign.getLine(1).substring(2), plugin);
		this.sign = sign;
	}
	
	public int calculateArea(){
		return plugin.util.getArea(plugin.worldguard.getRegionManager(this.sign.getWorld()).getRegion(region_id));
	}

	public void setDefaultPrice(){
		this.price=plugin.getConfig().getInt("price_per_block")*this.area;
	}
	
	private boolean checkRegistered(){
		if(plugin.persistenceHandler.loadEstate(this.region_id)==null){
			return false;
		} else {
			return true;
		}
	}
	
	public boolean create(Sign sign) {
															
		this.sign = sign;
		this.location = this.sign.getLocation();
		this.world = this.sign.getWorld().getName();
		if (this.registered == true) {
			System.out.println(plugin.locMan.get("alreadyRegistered"));
			return false;
		}
		if (area == 0) {
			calculateArea();
		}
		
		plugin.persistenceHandler.saveEstate(this);
		
		this.registered = true;
		plugin.sign_util.updateSign(sign, plugin.locMan.get("forSale"), this.region_id,
					plugin.getConfig().getString("Bank", "Bank"),
					String.valueOf(this.price));
//		} else {
//			plugin.sign_util.updateSign(sign, "Zu vermieten:", this.region_id,
//					plugin.getConfig().getString("Bank", "Bank"),
//					String.valueOf(this.rent));
//		}
		
		if (plugin.getConfig().getBoolean("Save_Buy",true)) {
			saveState();
		}
		return true;
	}


	public boolean repair(Sign sign) { // repair: Repariere das
														// aktuelle Grundst�ck
														// in der Datenbank

		this.sign = sign;
		this.location = this.sign.getLocation();
		this.world = this.sign.getWorld().getName();
		if (this.registered == true) {
			return false;
		}
		if (area == 0) {
			calculateArea();
		}

		plugin.persistenceHandler.saveEstate(this);
		
		this.registered = true;

		plugin.sign_util.updateSign(sign, "", this.region_id, owner, "");
		return true;
	}

	public boolean remove() { // remove: L�sche das Grundst�ck
		if (sign.getLine(1).length() >= 3) {
			deleteState();
			plugin.getDatabase().delete(this);
			return true;
		} else {
			return false;
		}

	}

	public boolean sell() { // sell: Routine zum Verkauf des Grundst�ckes

		plugin.persistenceHandler.saveEstate(this);

		plugin.util.regionChangeOwner(this.getRegionId());
		//plugin.file_util.updateStats(this.getSeller(), true);
		plugin.sign_util.updateSign(this.sign, plugin.locMan.get("forSale"),
				this.getRegionId(), this.getOwner(),
				String.valueOf(this.getPrice()));
		restoreState();
		return true;
	}
	
	public boolean isRentable(){
		return false;
	}

	public boolean buy(Player player) { // buy: Routine zum Kauf

		// ############# Wenn was im Preis steht #################
		if (getPrice() >= 0) {

			// int temp = (int) (player.getLocation().getX() * 4);
			// temp += player.getLocation().getZ();
			// if ((plugin.confirm.get(player.getName()) == null) ||
			// (plugin.confirm.get(player.getName()) != temp)) {
			// player.sendMessage("Klicke ein weiteres mal zur Best�tigung...");
			// plugin.confirm.put(player.getName(), temp);
			// return false;
			//
			// } else {

			Holdings balance_buy = new Holdings(player.getName());
			Holdings balance_sell = new Holdings(this.getOwner());

			int price = getPrice();
			if (!balance_buy.hasEnough(price)
					&& !(getOwner().equals(player.getName()))) {
				player.sendMessage(ChatColor.RED + plugin.locMan.get("notEnoughMoney"));
				balance_sell = null;
				balance_buy = null;
				return false;
			}

			// System.out.println(price);
			balance_sell.add(price);
			balance_buy.subtract(price);

			balance_sell = null;
			balance_buy = null;
			
			this.owner = player.getName();

			plugin.getDatabase().save(this);
			
			plugin.gs_start.remove(player.getName());
			plugin.util.regionChangeOwner(this.getRegionId(), player);
			//plugin.file_util.updateStats(player.getName(), false);
			plugin.sign_util.updateSign(
					(Sign) plugin.getServer().getWorld(this.getWorld())
							.getBlockAt(this.getLocation()).getState(), "",
					this.getRegionId(), player.getName(), "");
			return true;
		} else {
			return false;
		}
		// }

	}

	

	protected void deleteState() {

		File sch = new File(plugin.getDataFolder() + File.separator
				+ "schematics" + File.separator + this.getRegionId()
				+ ".schematic");
		sch.delete();
	}

	protected void restoreState() {

		File sch = new File(plugin.getDataFolder() + File.separator
				+ "schematics" + File.separator + this.getRegionId()
				+ ".schematic");
		if (!sch.exists()) {
			return;
		}
		SchematicFormat format = SchematicFormat.getFormat("MCEdit");
		EditSession es = new EditSession(new BukkitWorld(plugin.getServer()
				.getWorld(this.getWorld())), 1000000);
		CuboidClipboard cubo;
		try {
			cubo = format.load(sch);
			cubo.paste(es,
					new Vector(this.getLocation().getBlockX(), this
							.getLocation().getBlockY(), this.getLocation()
							.getBlockZ()), false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected boolean saveState() {
		File path = new File(plugin.getDataFolder() + File.separator
				+ "schematics" + File.separator);
		World world = plugin.getServer().getWorld(this.getWorld());
		ProtectedRegion region = plugin.worldguard.getRegionManager(world)
				.getRegion(this.getRegionId());

		Vector min = region.getMinimumPoint();
		Vector max = region.getMaximumPoint();
		EditSession session = new EditSession(new BukkitWorld(world), 10000000);

		CuboidClipboard cubo = new CuboidClipboard(max.subtract(min).add(
				new Vector(1, 1, 1)), min, min.subtract(new Vector(this
				.getLocation().getBlockX(), this.getLocation().getBlockY(),
				this.getLocation().getBlockZ())));
		cubo.copy(session);
		SchematicFormat format;
		if (SchematicFormat.getFormats().size() == 1) {
			format = SchematicFormat.getFormats().iterator().next();
		} else {
			System.out
					.println("More than one schematic format is available. Please provide the desired format");
			return false;
		}

		if (!path.exists()) {
			path.mkdir();
		}

		File sch = new File(path.getAbsolutePath() + File.separator
				+ this.getRegionId() + ".schematic");
		/*
		 * System.out.println(cubo.getOrigin().getBlockX());
		 * System.out.println(cubo.getOrigin().getBlockY());
		 * System.out.println(cubo.getOrigin().getBlockZ());
		 * System.out.println(cubo.getOffset().getBlockX());
		 * System.out.println(cubo.getOffset().getBlockY());
		 * System.out.println(cubo.getOffset().getBlockZ());
		 * System.out.println(cubo.getWidth());
		 * System.out.println(cubo.getLength());
		 * System.out.println(cubo.getHeight());
		 * System.out.println(cubo.getPoint(new BlockVector(1, 1, 1)));
		 * System.out.println(sch);
		 */
		try {
			format.save(cubo, sch);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

//	public boolean isExpired() { 
//
//		if (this.expires.before(new Timestamp(System.currentTimeMillis()))) {
//			return true;
//		} else {
//			return false;
//		}
//	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getPrice() {
		return this.price;
	}

	public int getArea() {
		return this.area;
	}

	public void setArea(int area) {
		this.area=area;
	}
	
	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Location getLocation() {
		return this.location;
	}

	public String getRegionId() {
		return this.region_id;
	}

	public String getWorld() {
		return this.world;
	}

	public ProtectedRegion getRegion() {
		return this.region;
	}

}
