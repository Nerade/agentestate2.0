package de.blockschmiede.agentestate.estate;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.iCo6.system.Holdings;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.persistence.RentableBean;

public class Rentable extends Estate {

	private String renter;
	private int rent, period;
	private long expiration;

	public Rentable(RentableBean bean, AgentEstate plugin) {
		super(bean.getRegion_id(), plugin);
		this.registered = true;
		this.setArea(bean.getArea());
		this.setOwner(bean.getOwner());
		this.setRenter(bean.getRenter());
		this.setRent(bean.getRent());
		this.setWorld(bean.getWorld());
		this.setLocation(this.plugin.getServer().getWorld(bean.getWorld()),
				bean.getX(), bean.getY(), bean.getZ());
	}

	public Rentable(String region_id, AgentEstate plugin) {
		super(region_id, plugin);
	}

	public Rentable(Sign sign, AgentEstate plugin) {
		super(sign, plugin);
	}

	public boolean create(Sign sign) {

		this.sign = sign;
		this.location = this.sign.getLocation();
		this.world = this.sign.getWorld().getName();
		if (this.registered == true) {
			System.out.println(plugin.locMan.get("alreadyRegistered"));
			return false;
		}
		if (area == 0) {
			calculateArea();
		}

		plugin.persistenceHandler.saveRentable(this);

		this.registered = true;
		plugin.sign_util.updateSign(sign, plugin.locMan.get("forRent"),
				this.region_id, this.getOwner(), String.valueOf(this.rent));

		if (plugin.getConfig().getBoolean("Save_Buy", true)) {
			saveState();
		}
		return true;
	}

	public boolean rent(Player player) { // rent: Routine zum Mieten

		Holdings balance = new Holdings(player.getName());
		Holdings bank = new Holdings(this.getOwner());
		if (balance.hasEnough(this.getRent())) {

			balance.subtract(this.getRent());
			bank.add(this.getRent());

			this.renter = player.getName();
			this.expiration = System.currentTimeMillis() + this.getPeriod()
					* 24 * 3600 * 1000;

			plugin.persistenceHandler.saveRentable(this);

			plugin.util.regionChangeMember(this.getRegionId(), player);

			plugin.sign_util.updateSign(
					(Sign) plugin.getServer().getWorld(this.getWorld())
							.getBlockAt(this.getLocation()).getState(),
					plugin.locMan.get("rented"), this.getRegionId(),
					player.getName(), "");
			return true;
		} else {
			player.sendMessage(ChatColor.RED
					+ plugin.locMan.get("notEnoughMoney"));
		}
		return false;
	}

	public boolean release() { // release: Enteigne Mieter

		this.expiration = 0;
		this.renter = null;

		plugin.persistenceHandler.saveRentable(this);

		plugin.util.regionChangeMember(this.getRegionId());
		plugin.sign_util.updateSign(
				(Sign) plugin.getServer().getWorld(this.getWorld())
						.getBlockAt(this.getLocation()).getState(),
				plugin.locMan.get("forRent"), this.getRegionId(),
				this.getOwner(), String.valueOf(this.rent));
		restoreState();
		return true;
	}

	
	
	@Override
	public boolean isRentable() {
		return true;
	}

	public String getRenter() {
		return renter;
	}

	public void setRenter(String renter) {
		this.renter = renter;
	}

	public int getRent() {
		return rent;
	}

	public void setDefaultRent() {
		this.rent = plugin.getConfig().getInt("Default_Period", 7);
	}

	public void setRent(int rent) {
		this.rent = rent;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public long getExpiration() {
		// TODO Auto-generated method stub
		return this.expiration;
	}

}
