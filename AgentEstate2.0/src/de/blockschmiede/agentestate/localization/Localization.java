package de.blockschmiede.agentestate.localization;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import de.blockschmiede.agentestate.AgentEstate;

public class Localization {
	
	private String locale;
	private AgentEstate plugin;
	private FileConfiguration langConfig = null;
	private File langFile = null;
	
	public Localization(String locale,AgentEstate plugin){
		this.locale = locale;
		this.plugin = plugin;
		getCustomConfig(locale + ".lang");
	}
	
	private void initializeLang(){
		final Map<String, String> defParams = new HashMap<String, String>();
		defParams.put("repairedEntries", "%1 entries have been repaired in database.");
		defParams.put("forSale", "For Sale:");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "yes");
		defParams.put("", "");
		   
		  for (final Entry<String, String> e : defParams.entrySet())
		    if (!langConfig.contains(e.getKey()))
		      langConfig.addDefault(e.getKey(), e.getValue());
		this.saveCustomConfig();
	}
	
	public String get(String key){
		return langConfig.getString(key,"Localized string not found!");
	}
	public String get(String key,Object... args){
		String raw = langConfig.getString(key);
		for(int i=0;i<args.length;i++){
			raw.replace("%" + String.valueOf(i+1), String.valueOf(args[i]));
		}
		return raw;
	}
	
	
	private void reloadCustomConfig(String filename) {
		if (langFile == null) {
			langFile = new File(plugin.getDataFolder(), filename);
		}
		langConfig = YamlConfiguration.loadConfiguration(langFile);

		// Look for defaults in the jar
		InputStream defConfigStream = plugin.getResource(filename);
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);
			langConfig.setDefaults(defConfig);
		}
	}

	private FileConfiguration getCustomConfig(String filename) {
		if (langConfig == null) {
			reloadCustomConfig(filename);
		}
		return langConfig;
	}
	
	public void saveCustomConfig() {
		if (langConfig == null || langFile == null) {
			return;
		}
		try {
			langConfig.save(langFile);
		} catch (IOException ex) {
			Logger.getLogger(JavaPlugin.class.getName()).log(Level.SEVERE,
					"Could not save language config to " + langFile, ex);
		}
	}

}
