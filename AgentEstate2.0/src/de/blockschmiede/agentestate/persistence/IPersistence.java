package de.blockschmiede.agentestate.persistence;

import de.blockschmiede.agentestate.estate.Estate;
import de.blockschmiede.agentestate.estate.Rentable;

public interface IPersistence {
	
	public Estate loadEstate(String regionID);
	public Rentable loadRentable(String regionID);
	public void saveEstate(Estate curEstate);
	public void saveRentable(Rentable curRent);

}
