package de.blockschmiede.agentestate.persistence;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.PersistenceException;

import org.bukkit.plugin.java.JavaPlugin;

import com.avaje.ebean.Query;

import de.blockschmiede.agentestate.AgentEstate;
import de.blockschmiede.agentestate.estate.Estate;
import de.blockschmiede.agentestate.estate.Rentable;

public class PersistenceDatabase implements IPersistence {
	
	private final AgentEstate plugin;
	
	public PersistenceDatabase(AgentEstate plugin){
		this.plugin = plugin;
		checkDDL();
	}

	private void checkDDL() {
		
		try{
			plugin.getDatabase().find(EstateBean.class).findRowCount();
		} catch (PersistenceException e){
			plugin.installDDL();
		}
		
	}

	@Override
	public Estate loadEstate(String regionID) {

		Query<EstateBean> query = plugin.getDatabase().find(EstateBean.class);
		query.where().eq("region_id", regionID);
		query.findUnique();
		List<EstateBean> beans = query.findList();
		if(beans==null||beans.size()==0){
			return null;
		}
		EstateBean bean = beans.get(0);
		Estate es = new Estate(bean,plugin);
				
		return es;
	}

	@Override
	public void saveEstate(Estate curEstate) {
		EstateBean bean = new EstateBean();
		bean.setRegion_id(curEstate.getRegionId());
		bean.setOwner(curEstate.getOwner());
		bean.setPrice(curEstate.getPrice());
		bean.setArea(curEstate.getArea());
		bean.setWorld(curEstate.getWorld());
		bean.setX(curEstate.getLocation().getBlockX());
		bean.setY(curEstate.getLocation().getBlockY());
		bean.setZ(curEstate.getLocation().getBlockZ());
		
		plugin.getDatabase().save(bean);
	}

	@Override
	public Rentable loadRentable(String regionID) {

		Query<RentableBean> query = plugin.getDatabase().find(RentableBean.class);
		query.where().eq("region_id", regionID);
		query.findUnique();
		List<RentableBean> beans = query.findList();
		if(beans==null||beans.size()==0){
			return null;
		}
		RentableBean bean = beans.get(0);
		Rentable es = new Rentable(bean,plugin);
				
		return es;
		
	}

	@Override
	public void saveRentable(Rentable curRent) {

		RentableBean bean = new RentableBean();
		bean.setRegion_id(curRent.getRegionId());
		bean.setOwner(curRent.getOwner());
		bean.setRenter(curRent.getRenter());
		bean.setRent(curRent.getRent());
		bean.setPeriod(curRent.getPeriod());
		bean.setExpires(new Timestamp(curRent.getExpiration()));
		bean.setArea(curRent.getArea());
		bean.setWorld(curRent.getWorld());
		bean.setX(curRent.getLocation().getBlockX());
		bean.setY(curRent.getLocation().getBlockY());
		bean.setZ(curRent.getLocation().getBlockZ());
		
		plugin.getDatabase().save(bean);
		
	}

}
